import javax.swing.text.StyledEditorKit;
import java.io.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;

public class TaskFileManager {
    public static void writeTaskInfoToFile(Task task, String filePath) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath, true))) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

            writer.write("Task ID: " + task.getId());
            writer.newLine();
            writer.write("Header: " + task.getHeader());
            writer.newLine();
            writer.write("Description: " + task.getDescription());
            writer.newLine();
            writer.write("Deadline: " + (task.getDeadline() != null ? task.getDeadline().format(formatter) : "Not Set"));
            writer.newLine();
            writer.write("Priority: " + task.getPriority());
            writer.newLine();

            // Write Parent Task info
            if (task.getParentTask() != null) {
                writer.write("Parent Task ID: " + task.getParentTask().getId());
                writer.newLine();
                writer.write("Parent Task Name: " + task.getParentTask().getHeader());
                writer.newLine();
            } else {
                writer.write("No Parent Task");
                writer.newLine();
            }

            // Write Children Tasks info
            if (task.getChildTasks().isEmpty()) {
                writer.write("No Child Tasks");
                writer.newLine();
            } else {
                writer.write("Child Tasks:");
                writer.newLine();
                for (Task childTask : task.getChildTasks()) {
                    writer.write("  Child Task ID: " + childTask.getId());
                    writer.newLine();
                    writer.write("  Child Task Name: " + childTask.getHeader());
                    writer.newLine();
                    writer.write("  ------------------------------");
                    writer.newLine();
                }
            }
            writer.write("!");  // Add '!' to indicate the end of the task info
            writer.newLine();
        } catch (IOException e) {
            System.err.println("Error writing task info to file: " + e.getMessage());
        }
    }
    public static void readTasksFromFile(Collection<Task> tasks, String filePath) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;

            while ((line = reader.readLine()) != null) {
                Task task = createTaskFromLine(line, reader);
                if (task != null) {
                    tasks.add(task);
                }
            }
        } catch (IOException e) {
            System.err.println("Error reading tasks from file: " + e.getMessage());
        }
    }
    private static Task createTaskFromLine(String firstLine, BufferedReader reader) throws IOException {
        // Assuming each task info block starts with "Task ID: "
        if (!firstLine.startsWith("Task ID: ")) {
            return null;
        }

        Task task = new Task();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        // Read and set Task ID
        task.setId(Integer.parseInt(firstLine.replace("Task ID: ", "")));

        task.setHeader(reader.readLine().replace("Header: ", ""));
        task.setDescription(reader.readLine().replace("Description: ", ""));

        String deadlineLine = reader.readLine().replace("Deadline: ", "");
        task.setDeadline(deadlineLine.equals("Not Set") ? null : LocalDateTime.parse(deadlineLine, formatter));

        task.setPriority(Task.Priority.valueOf(reader.readLine().replace("Priority: ", "")));

        // Read Parent Task info
        String parentTaskIdLine = reader.readLine().replace("Parent Task ID: ", "");
        if (!parentTaskIdLine.equals("No Parent Task")) {
            Task parentTask = new Task();
            parentTask.setId(Integer.parseInt(parentTaskIdLine));
            parentTask.setHeader(reader.readLine().replace("Parent Task Name: ", ""));
            task.setParentTask(parentTask);
        }

        // Read Children Tasks info
        String line; // Declare line variable
        String childTasksHeader = reader.readLine();
        if (childTasksHeader.equals("Child Tasks:")) {
            while (!(line = reader.readLine()).equals("!")) {
                Task childTask = new Task();
                childTask.setId(Integer.parseInt(line.replaceAll("\\D", "")));
                childTask.setHeader(reader.readLine().replace("  Child Task Name: ", ""));
                task.addChildTask(childTask);

                // Consume the line separating child tasks
                reader.readLine();
            }
        }

        return task;
    }
}
