import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TaskScheduler {
    public static void scheduleTasksForTheWeek(Collection<Task> tasks) {
        LocalDateTime today = LocalDateTime.now();
        Map<DayOfWeek, List<Task>> tasksByDay = new HashMap<>();
        List<Task> tasksLater = new ArrayList<>();

        // Initialize the tasksByDay map with empty lists for each day of the week
        for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
            tasksByDay.put(dayOfWeek, new ArrayList<>());
        }

        // Group tasks by day of the week
        for (Task task : tasks) {
            LocalDateTime taskDeadline = task.getDeadline();
                if ( taskDeadline == null || today.plusDays(6).isBefore(taskDeadline)) {
                    tasksLater.add(task); // Add tasks with deadlines further than 6 days to the 'Later' column
                } else {
                    DayOfWeek taskDayOfWeek = taskDeadline.getDayOfWeek();
                    tasksByDay.get(taskDayOfWeek).add(task);
                }

        }

        // Print tasks for today and the next 6 days
        for (int i = 0; i < 7; i++) {
            LocalDateTime currentDate = today.plusDays(i);
            DayOfWeek currentDayOfWeek = currentDate.getDayOfWeek();
            List<Task> tasksForCurrentDay = tasksByDay.get(currentDayOfWeek);

            DateUtil.printFormattedDate(currentDate);

            if (!tasksForCurrentDay.isEmpty()) {
                sortTasks(tasksForCurrentDay); // Sort tasks for the day
                for (Task task : tasksForCurrentDay) {
                    task.printTaskInfo();
                    System.out.println("------------------------");
                }
            } else {
                System.out.println("No tasks for this day");
            }
        }

        // Print tasks with deadlines further than 6 days in the 'Later' column
        if (!tasksLater.isEmpty()) {
            System.out.println("\nLater:");

            sortTasks(tasksLater); // Sort tasks in the 'Later' column
            for (Task task : tasksLater) {
                System.out.println( "#" + task.getId() + " " + task.getHeader());
                System.out.println("------------------------");
            }
        }
    }

    private static void sortTasks(List<Task> tasks) {
        // Partition tasks into two lists: tasksWithDeadline and tasksWithoutDeadline
        Map<Boolean, List<Task>> partitionedTasks = tasks.stream()
                .collect(Collectors.partitioningBy(task -> task.getDeadline() != null));

        // Sort tasks with non-null deadlines
        List<Task> tasksWithDeadline = partitionedTasks.get(true);
        tasksWithDeadline.sort(Comparator.comparing(Task::getDeadline)
                .thenComparing(Task::getPriority, Comparator.reverseOrder()));

        // Combine sorted tasks with non-null deadlines and tasks with null deadlines
        List<Task> sortedTasks = new ArrayList<>(tasksWithDeadline);
        sortedTasks.addAll(partitionedTasks.get(false));

        // Replace the original tasks list with the sorted list
        tasks.clear();
        tasks.addAll(sortedTasks);
    }
}
