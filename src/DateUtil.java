import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;

public class DateUtil {
    public static void printFormattedDate(LocalDateTime dateTime) {
        LocalDate today = LocalDate.now();
        LocalDate inputDate = dateTime.toLocalDate();

        String dayOfWeek = formatDayOfWeek(inputDate.getDayOfWeek());
        String formattedDate = formatDate(dateTime.toLocalDate());

        String underline = "-".repeat(dayOfWeek.length() + formattedDate.length() + 4); // 4 is for spaces and separators

        if (inputDate.equals(today)) {
            System.out.println(underline);
            System.out.println(dayOfWeek + " | " + formattedDate + " (Today)");
        } else {
            System.out.println(underline);
            System.out.println(dayOfWeek + " | " + formattedDate);
        }
        System.out.println(underline);
    }

    public static String formatDayOfWeek(DayOfWeek dayOfWeek) {
        return dayOfWeek.getDisplayName(TextStyle.FULL, Locale.ENGLISH);
    }

    public static String formatDate(LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return date.format(formatter);
    }

    public static String fomatDateTime(LocalDateTime dateTime){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return  dateTime.format(formatter);
    }

    public static DateTimeFormatter getFormatterDateTime(){
        return DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    }
}