import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashSet;

public class Task {
    private int id;
    private String header;
    private String description;
    private LocalDateTime deadline;
    private Priority priority;
    private Task parentTask;
    private LinkedHashSet<Task> childTasks;

    public Task() {
        this.childTasks = new LinkedHashSet<>();
    }

    public Task(int id, String header, String description, LocalDateTime deadline, Priority priority) {
        this.id = id;
        this.header = header;
        this.description = description;
        this.deadline = deadline;
        this.priority = priority;
        this.childTasks = new LinkedHashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void appendToDescription(String additionalText) {
        if (additionalText != null) {
            if (this.description == null) {
                this.description = additionalText;
            } else {
                this.description += "\n" + additionalText;
            }
        }
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public Task getParentTask() {
        return parentTask;
    }

    public void setParentTask(Task parentTask) {
        this.parentTask = parentTask;
    }

    public LinkedHashSet<Task> getChildTasks() {
        return childTasks;
    }

    public void addChildTask(Task task) {
        this.childTasks.add(task);
    }

    public void removeChildTask(Task childTask) {
        if (childTasks != null) {
            childTasks.remove(childTask);
        }
    }

    public void printTaskInfo() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        System.out.println("Task ID: " + getId());
        System.out.println("Header: " + getHeader());
        System.out.println("Description: " + getDescription());
        System.out.println("Deadline: " + (getDeadline() != null ? getDeadline().format(formatter) : "Not Set"));
        System.out.println("Priority: " + getPriority());

        // Print Parent Task info
        if (getParentTask() != null) {
            System.out.println("Parent Task ID: " + getParentTask().getId());
            System.out.println("Parent Task Name: " + getParentTask().getHeader());
        } else {
            System.out.println("No Parent Task");
        }

        // Print Children Tasks info
        if (getChildTasks().isEmpty()) {
            System.out.println("No Child Tasks");
        } else {
            System.out.println("Child Tasks:");
            for (Task childTask : getChildTasks()) {
                System.out.println("  Child Task ID: " + childTask.getId());
                System.out.println("  Child Task Name: " + childTask.getHeader());
                System.out.println("  ------------------------------");
            }
        }
    }

    public static enum Priority {
        LOW, MEDIUM, HIGH
    }
}
