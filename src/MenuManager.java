import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class MenuManager {
    public static void displayMenu() {
        System.out.println("Main Menu:");
        System.out.println("1. Create Task");
        System.out.println("2. Search Task by ID");
        System.out.println("3. Remove Task by ID");
        System.out.println("4. Alter Task Description and Deadline");
        System.out.println("5. Create Child Task");
        System.out.println("6. Display All Tasks");
        System.out.println("7. Display Weekly Plan");
        System.out.println("8. Exit");
        System.out.print("Enter your choice: ");
    }

    public static Task createTaskFromUserInput(Organizer organizer) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Creating a new task. Please provide the following information:");

        System.out.print("ID: ");
        int id = scanner.nextInt();
        while (true) {
            if (organizer.getTakenIds().contains(id)) {
                System.out.print("Such id already exists. Enter other ID: ");
                id = scanner.nextInt();
            } else {
                break;
            }
        }

        scanner.skip("\n");
        //here add check on id

        System.out.print("Header: ");
        String header = scanner.nextLine();

        System.out.print("Description: ");
        String description = scanner.nextLine();

        System.out.print("Deadline (Format: dd-MM-yyyy HH:mm:ss, or press Enter for no deadline): ");
        String deadlineInput = scanner.nextLine();
        LocalDateTime deadline = null;
        if (!deadlineInput.isEmpty()) {
            deadline = LocalDateTime.parse(deadlineInput, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
            ZonedDateTime zonedDateTime = deadline.atZone(organizer.getUserTimeZone());

        }

        System.out.print("Priority (LOW, MEDIUM, HIGH): ");
        Task.Priority priority = Task.Priority.valueOf(scanner.nextLine().toUpperCase());
        Task newTask = new Task(id, header, description, deadline, priority);
        organizer.addTask(newTask);
        System.out.println("Task created!\n");
        return newTask;
    }

    public static void searchTaskByIdAndPrintInfo(Organizer organizer) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter Task ID to search: ");
        int taskIdToSearch = scanner.nextInt();

        Task foundTask = organizer.searchTaskById(taskIdToSearch);

        if (foundTask != null) {
            System.out.println("Task found. Task Info:");
            System.out.println("------");
            foundTask.printTaskInfo();
            System.out.println("------");
        } else {
            System.out.println("Task with ID " + taskIdToSearch + " not found.");
        }
    }

    public static void removeTaskById(Organizer organizer) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter Task ID to remove: ");
        int taskIdToRemove = scanner.nextInt();

        Task taskToRemove = organizer.searchTaskById(taskIdToRemove);

        if (taskToRemove != null) {
            // Remove from parent task's childTasks
            if (taskToRemove.getParentTask() != null) {
                taskToRemove.getParentTask().removeChildTask(taskToRemove);
            }

            // Update parent field for child tasks
            for (Task childTask : taskToRemove.getChildTasks()) {
                childTask.setParentTask(null);
            }
            // Remove the task from the main task list
            organizer.getTasks().remove(taskToRemove);

            System.out.println("Task with ID " + taskIdToRemove + " removed successfully.");
        } else {
            System.out.println("Task with ID " + taskIdToRemove + " not found. No task removed.");
        }
    }

    public static void createChildTaskFromUserInput(Organizer organizer) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Creating a new child task. Please provide the following information:");

        // Prompt user for parent task ID
        System.out.print("Parent Task ID: ");
        int parentId = scanner.nextInt();

        // Check if parent task exists
        Task parentTask = organizer.searchTaskById(parentId);
        if (parentTask == null) {
            System.out.println("Parent task with ID " + parentId + " does not exist.");
            return;
        }

        // Create and add child task
        Task childTask = createTaskFromUserInput(organizer);
        childTask.setParentTask(parentTask);
        parentTask.addChildTask(childTask);

        System.out.println("Child task created and added to parent task!\n");
    }

    public static void alterTaskDescriptionAndDeadline(Organizer organizer) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Altering task description and deadline. Please provide the following information:");

        // Prompt user for task ID
        System.out.print("Task ID: ");
        int taskId = scanner.nextInt();

        // Check if task exists
        Task task = organizer.searchTaskById(taskId);
        if (task == null) {
            System.out.println("Task with ID " + taskId + " does not exist.");
            return;
        }

        // Edit description
        System.out.print("Do you want to edit the description? (yes/no): ");
        String editDescription = scanner.next().toLowerCase();
        if (editDescription.equals("yes")) {
            System.out.println("Previous Description: " + task.getDescription());
            System.out.print("Enter a new description: ");
            scanner.nextLine();
            String newDescription = scanner.nextLine();
            task.setDescription(newDescription);
            System.out.println("Task description altered!\n");
        }

        // Edit deadline
        System.out.print("Do you want to edit the deadline? (yes/no): ");
        String editDeadline = scanner.next().toLowerCase();
        if (editDeadline.equals("yes")) {
            System.out.println("Previous Deadline: " + (task.getDeadline() != null ? task.getDeadline().format(DateUtil.getFormatterDateTime()) : "Not Set"));
            System.out.print("Enter a new deadline (Format: dd-MM-yyyy HH:mm:ss) or press Enter for no deadline: ");
            scanner.nextLine();
            String newDeadlineInput = scanner.nextLine();
            if (!newDeadlineInput.isEmpty()) {
                LocalDateTime newDeadline = LocalDateTime.parse(newDeadlineInput, DateUtil.getFormatterDateTime());
                task.setDeadline(newDeadline);
            } else {
                task.setDeadline(null);
            }
            System.out.println("Task deadline altered!\n");
        }
    }

    public static void notifyOnClosingDeadlines(Organizer organizer) {
        LocalDateTime currentDateTime = LocalDateTime.now();
        ZoneId userTimeZone = organizer.getUserTimeZone();
        boolean hasClosingDeadlines = false;

        for (Task task : organizer.getTasks()) {
            if (task.getDeadline() != null) {
                LocalDateTime deadline = task.getDeadline();

                // Convert task deadline to user's time zone
                ZonedDateTime deadlineInTimeZone = deadline.atZone(ZoneId.systemDefault()).withZoneSameInstant(userTimeZone);

                long hoursUntilDeadline = ChronoUnit.HOURS.between(currentDateTime, deadlineInTimeZone.toLocalDateTime());

                // Check if the deadline is within the next 24 hours
                if (hoursUntilDeadline <= 24 && hoursUntilDeadline > 0) {
                    System.out.println("#" + task.getId() + ": " + task.getHeader());
                    hasClosingDeadlines = true;
                }
            }
        }

        if (!hasClosingDeadlines) {
            System.out.println("No tasks with closing deadlines.");
        }
    }
}
