import java.io.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Scanner;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class TaskZoneManager {
    public static void updateTaskDeadline(Task task, LocalDateTime newDeadline, ZoneId userTimeZone) {
        if (newDeadline == null)
            return;
        ZonedDateTime zonedDateTime = newDeadline.atZone(userTimeZone);
        task.setDeadline(zonedDateTime.toLocalDateTime());
    }
    public static void updateTaskDeadlineTimeZone(ZoneId currentZoneId, ZoneId newZoneId, Task task) {
        if (task.getDeadline() != null) {
            LocalDateTime currentDeadline = task.getDeadline();
            ZonedDateTime currentZonedDateTime = ZonedDateTime.of(currentDeadline, currentZoneId);

            ZonedDateTime newZonedDateTime = currentZonedDateTime.withZoneSameInstant(newZoneId);
            LocalDateTime newDeadline = newZonedDateTime.toLocalDateTime();
            DateUtil.fomatDateTime(newDeadline);

            task.setDeadline(newDeadline);
        }
    }
    public static String displayTimeZone(TimeZone tz) {

        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                - TimeUnit.HOURS.toMinutes(hours);
        // avoid -4:-30 issue
        minutes = Math.abs(minutes);

        String result = "";
        if (hours > 0) {
            result = String.format("(GMT+%d:%02d) %s", hours, minutes, tz.getID());
        } else {
            result = String.format("(GMT%d:%02d) %s", hours, minutes, tz.getID());
        }

        return result;
    }
    public static ZoneId setUserTimeZone() {
        Scanner scanner = new Scanner(System.in);
        ZoneId userTimeZone;
        System.out.println("Enter your preferred timezone (e.g., America/New_York): ");
        String timeZoneId = scanner.nextLine();

        try {
            // Validate and set the user's preferred timezone
            ZoneId zoneId = ZoneId.of(timeZoneId);
            userTimeZone = zoneId;
            System.out.println("Timezone set to: " + zoneId);
        } catch (Exception e) {
            userTimeZone = ZoneId.systemDefault();
            System.out.println("Invalid timezone. Using the default system timezone.");
        }

        return userTimeZone;
    }
    public static void writeTimeZoneToFile(ZoneId zoneId, String filePath) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath, true))) {
            writer.write("TimeZone: " + zoneId.getId());
            writer.newLine();
        } catch (IOException e) {
            System.err.println("Error writing time zone to file: " + e.getMessage());
        }
    }
    public static ZoneId readZoneId(String filePath) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line = reader.readLine();
            if (line != null && line.startsWith("TimeZone: ")) {
                String timeZoneId = line.substring("TimeZone: ".length());
                if (isValidTimeZone(timeZoneId)) {
                    return ZoneId.of(timeZoneId);
                } else {
                    System.out.println("Invalid time zone: " + timeZoneId);
                    System.out.println("Using default system time zone");
                }
            }
        } catch (IOException e) {
            System.err.println("Error reading time zone from file: " + e.getMessage());
        }

        // Default to system time zone if reading fails or the time zone is invalid
        return ZoneId.systemDefault();
    }
    private static boolean isValidTimeZone(String timeZoneId) {
        String[] availableZoneIds = TimeZone.getAvailableIDs();
        for (String id : availableZoneIds) {
            if (id.equals(timeZoneId)) {
                return true;
            }
        }
        return false;
    }
}