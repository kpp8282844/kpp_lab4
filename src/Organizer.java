import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

public class Organizer {
    private Collection<Task> tasks;

    private List<Integer> takenIds;

    private ZoneId userTimeZone;

    private final String filename = "tasks.txt";

    public Organizer() {
        this.tasks = new LinkedHashSet<>();
        this.takenIds = new ArrayList<>();
        this.userTimeZone = ZoneId.systemDefault();
    }

    public void addTasks(Collection<Task> newTasks) {
        tasks.addAll(newTasks);
    }

    public void addTask(Task task) {
        tasks.add(task);
        takenIds.add(task.getId());
    }

    public Collection<Task> getTasks() {
        return tasks;
    }

    public List<Integer> getTakenIds() {
        return takenIds;
    }

    public void displayAllTasks() {
        System.out.println("All Tasks:");
        for (Task task : tasks) {
            task.printTaskInfo();
            System.out.println("------------------------");
        }
    }

    public Task searchTaskById(int taskId) {
        for (Task task : tasks) {
            if (task.getId() == taskId) {
                return task;
            }
        }
        return null; // Task with the specified ID not found
    }

    public void displayWeekTasks()
    {
        TaskScheduler.scheduleTasksForTheWeek(tasks);
    }

    public void readFromFile() {
        ZoneId currentZoneId = userTimeZone;
        userTimeZone = TaskZoneManager.readZoneId(filename);
        tasks.forEach(task -> TaskZoneManager.updateTaskDeadlineTimeZone(currentZoneId, userTimeZone, task));
        TaskFileManager.readTasksFromFile(tasks, filename);

        // Clear non-existent parent and child links
        Iterator<Task> iterator = tasks.iterator();
        while (iterator.hasNext()) {
            Task task = iterator.next();

            if (task.getParentTask() != null && !tasks.contains(task.getParentTask())) {
                task.setParentTask(null);
            }

            Iterator<Task> childIterator = task.getChildTasks().iterator();
            while (childIterator.hasNext()) {
                Task childTask = childIterator.next();
                if (!tasks.contains(childTask)) {
                    childIterator.remove();
                }
            }

            // If the task itself doesn't exist, remove it from the tasks list
            if (!tasks.contains(task)) {
                iterator.remove();
            }
        }

        tasks.forEach(task -> takenIds.add(task.getId()));
    }

    public void writeToFile(Task task)
    {
        TaskFileManager.writeTaskInfoToFile(task, filename);
    }

    private void writeTimeZone(){
        TaskZoneManager.writeTimeZoneToFile(userTimeZone, filename);
    }

    public void saveToFile()  {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename, false))) {
        } catch (IOException e) {
            System.err.println("Error erasing file content: " + e.getMessage());
        }
        writeTimeZone();
        tasks.forEach(this::writeToFile);
        System.out.println("File content saved successfully.");
    }

    public void setUserTimeZone(ZoneId timeZoneId) {
        userTimeZone = timeZoneId;
    }

    public ZoneId getUserTimeZone() {
        return userTimeZone;
    }

   public void updateTaskDeadline(Task task, LocalDateTime newDeadline) {
        TaskZoneManager.updateTaskDeadline(task, newDeadline, userTimeZone);
   }
}
