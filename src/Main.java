import java.time.ZoneId;
import java.util.*;

public class Main {
    public static void main(String[] args) {

        //fix problem with
        Organizer organizer = new Organizer();
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;

        organizer.readFromFile();

        ZoneId zoneId = organizer.getUserTimeZone();
        System.out.println("Set a new timezone? (Current: " + TaskZoneManager.displayTimeZone(TimeZone.getTimeZone(zoneId)) + ")");
        System.out.println("(yes/no):");
        String change = scanner.next().toLowerCase();
        if (change.equals("yes")) {
            ZoneId currentZoneId = organizer.getUserTimeZone();
            organizer.setUserTimeZone(TaskZoneManager.setUserTimeZone());
            //here updating deadlines of tasks
            organizer.getTasks().forEach(task ->
                    TaskZoneManager.updateTaskDeadlineTimeZone(currentZoneId, organizer.getUserTimeZone(), task));
        }
        else
            System.out.println("Keeping current timezone");


        while (!exit) {

            System.out.println("Tasks with Closing Deadlines:");
            MenuManager.notifyOnClosingDeadlines(organizer);
            MenuManager.displayMenu();
            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    MenuManager.createTaskFromUserInput(organizer);
                    break;
                case 2:
                    MenuManager.searchTaskByIdAndPrintInfo(organizer);
                    break;
                case 3:
                    MenuManager.removeTaskById(organizer);
                    break;
                case 4:
                    MenuManager.alterTaskDescriptionAndDeadline(organizer);
                    break;
                case 5:
                    MenuManager.createChildTaskFromUserInput(organizer);
                    break;
                case 6:
                    organizer.displayAllTasks();
                    break;
                case 7:
                    organizer.displayWeekTasks();
                    break;
                case 8:
                    exit = true;
                    break;
                default:
                    System.out.println("Invalid choice. Please enter a number between 1 and 8.");
            }
        }

        organizer.saveToFile();
        System.out.println("Exiting the program. Goodbye bro!");
    }
}